Slack Parser
============

This will parse Slack chat history export (a JSON file) into a CSV file for easier archival work/searching.
Point it towards a Slack-exported JSON file and it will create a file of the same name, but with a CSV extension.
The CSV file holds the messages and information on which user sent it and at what time it was sent - in an easily accessible interface.